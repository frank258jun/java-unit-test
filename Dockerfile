FROM anapsix/alpine-java:8_server-jre_unlimited

MAINTAINER yujianjun(xxxx@163.com)

ENV TZ=Asia/Shanghai

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /sca-test

WORKDIR /sca-test

EXPOSE 3000

ADD ./target/JavaUnitTest-1.0.0.jar ./