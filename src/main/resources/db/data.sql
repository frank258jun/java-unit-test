-- ----------------------------
--  Records of `sys_role`
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES ('1', 'ADMINROLE', 'ROLE_ADMIN', 'ADMIN', '0', '2', '2017-10-29 15:45:51', '2018-12-26 14:09:11', '0', '1');
INSERT INTO `sys_role` VALUES ('2', 'TESTROLE', 'ROLE_TEST', 'TESTA', '0', '2', '2017-10-29 15:45:51', '2018-12-26 14:09:11', '0', '1');
COMMIT;

-- ----------------------------
--  Records of `sys_user_role`
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('1', '2');
COMMIT;