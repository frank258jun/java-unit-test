drop table if exists sys_log;
drop table if exists sys_role;
drop table if exists sys_user_role;

-- ----------------------------
--  Table structure for `sys_log`
-- ----------------------------
CREATE TABLE sys_log (
  id bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  type char(1) DEFAULT '0' COMMENT '日志类型',
  title varchar(255) DEFAULT '' COMMENT '日志标题',
  service_id varchar(32) DEFAULT NULL COMMENT '服务ID',
  create_by varchar(64) DEFAULT NULL COMMENT '创建者',
  create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  update_time datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  remote_addr varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  user_agent varchar(1000) DEFAULT NULL COMMENT '用户代理',
  request_uri varchar(255) DEFAULT NULL COMMENT '请求URI',
  method varchar(10) DEFAULT NULL COMMENT '操作方式',
  params text COMMENT '操作提交的数据',
  time mediumtext COMMENT '执行时间',
  del_flag char(1) DEFAULT '0' COMMENT '删除标记',
  exception text COMMENT '异常信息',
  tenant_id int(11) DEFAULT '0' COMMENT '所属租户',
  PRIMARY KEY (`id`)
);

--- ----------------------------
--  Table structure for `sys_role`
-- ----------------------------
CREATE TABLE sys_role (
  role_id int(11) NOT NULL AUTO_INCREMENT,
  role_name varchar(64) NOT NULL,
  role_code varchar(64) NOT NULL,
  role_desc varchar(255) DEFAULT NULL,
  ds_type char(1) NOT NULL DEFAULT '2' COMMENT '数据权限类型',
  ds_scope varchar(255) DEFAULT NULL COMMENT '数据权限范围',
  create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  update_time timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  del_flag char(1) DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  tenant_id int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
);

-- ----------------------------
--  Table structure for `sys_user_role`
-- ----------------------------
CREATE TABLE sys_user_role (
  user_id int(11) NOT NULL COMMENT '用户ID',
  role_id int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) 
);