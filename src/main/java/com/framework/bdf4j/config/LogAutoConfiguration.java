package com.framework.bdf4j.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import com.framework.bdf4j.annotations.SysLogAspect;
import com.framework.bdf4j.event.SysLogListener;
import com.framework.bdf4j.sysadm.service.SysLogService;

import lombok.AllArgsConstructor;

/**
 * 日志自动配置
 */
@EnableAsync
@Configuration
@AllArgsConstructor
@ConditionalOnWebApplication
public class LogAutoConfiguration {

    private final SysLogService sysLogService;;
    
	@Bean
	public SysLogListener sysLogListener() {
		return new SysLogListener(sysLogService);
	}

	@Bean
	public SysLogAspect sysLogAspect(ApplicationEventPublisher publisher) {
		return new SysLogAspect(publisher);
	}
}
