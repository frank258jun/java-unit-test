package com.framework.bdf4j.config;

import java.util.List;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.framework.bdf4j.comm.persist.DataScopeHandle;
import com.framework.bdf4j.comm.persist.DataScopeInnerInterceptor;
import com.framework.bdf4j.comm.persist.DataScopeSqlInjector;
import com.framework.bdf4j.comm.persist.ScaDefaultDatascopeHandle;

@Configuration
@ConditionalOnBean(DataSource.class)
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
@MapperScan("com.framework.bdf4j.**.mapper")
public class MybatisPlusConfiguration implements  WebMvcConfigurer {
	
	@Override
    public void addViewControllers(ViewControllerRegistry registry ) {
        registry.addViewController("/").setViewName( "forward:/index" );
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    } 
    
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new SqlFilterArgumentResolver());
    }
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        /** swagger配置 */
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    
    /**
     * 默认数据权限处理器
     * 
     * @return ScaDefaultDatascopeHandle
     */
    @Bean
    @ConditionalOnMissingBean
    public DataScopeHandle dataScopeHandle() {
        return new ScaDefaultDatascopeHandle();
    }

    /**
     * mybatis plus 拦截器配置
     * 
     * @return ScaDefaultDatascopeHandle
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 数据权限
        DataScopeInnerInterceptor dataScopeInnerInterceptor = new DataScopeInnerInterceptor();
        dataScopeInnerInterceptor.setDataScopeHandle(dataScopeHandle());
        interceptor.addInnerInterceptor(dataScopeInnerInterceptor);
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.H2));
        return interceptor;
    }

    /**
     * 扩展 mybatis-plus baseMapper 支持数据权限
     * 
     * @return
     */
    @Bean
    @ConditionalOnBean(DataScopeHandle.class)
    public DataScopeSqlInjector dataScopeSqlInjector() {
        return new DataScopeSqlInjector();
    }

}
