package com.framework.bdf4j.event;

import com.framework.bdf4j.sysadm.entity.SysLog;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统日志事件
 */
@Getter
@AllArgsConstructor
public class SysLogEvent {
	private final SysLog sysLog;
}
