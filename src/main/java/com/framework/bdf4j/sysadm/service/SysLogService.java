package com.framework.bdf4j.sysadm.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.framework.bdf4j.sysadm.entity.SysLog;

/**
 * <p>
 * 日志表 服务类
 * </p>
 *
 */
public interface SysLogService extends IService<SysLog> {

    /**
     * 分页查询日志信息
     *
     * @param page
     *            分页对象
     * @param syslog
     *            参数列表
     * @return
     */
    IPage getSysLogPage(Page page, SysLog syslog);

    /**
     * 查询所有日志信息
     *
     * @param syslog
     *            参数列表
     * @return
     */
    List<SysLog> getSysLogList(SysLog syslog);
}
