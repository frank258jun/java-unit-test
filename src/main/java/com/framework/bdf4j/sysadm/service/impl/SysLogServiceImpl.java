package com.framework.bdf4j.sysadm.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.framework.bdf4j.sysadm.entity.SysLog;
import com.framework.bdf4j.sysadm.mapper.SysLogMapper;
import com.framework.bdf4j.sysadm.service.SysLogService;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 *
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

    /**
     * 分页查询日志信息
     *
     * @param page
     *            分页对象
     * @param syslog
     *            参数列表
     * @return
     */
    @Override
    public IPage getSysLogPage(Page page, SysLog syslog) {
        QueryWrapper<SysLog> ew = new QueryWrapper<SysLog>();
        ew.eq(StrUtil.isNotBlank(syslog.getType()), "type", syslog.getType())
            .like(StrUtil.isNotBlank(syslog.getTitle()), "title", syslog.getTitle())
            .likeRight(StrUtil.isNotBlank(syslog.getCreateBy()), "create_by", syslog.getCreateBy()).eq("del_flag", "0");
        try {
            if (StrUtil.isNotBlank(syslog.getBeginTime())) {
                ew.ge("create_time", DateUtil.parseDate(syslog.getBeginTime()));
            }
            if (StrUtil.isNotBlank(syslog.getEndTime())) {
                Date date = DateUtil.parseDate(syslog.getEndTime());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);
                ew.lt("create_time", calendar.getTime());
            }
        } catch (Exception e) {
            log.error("时间转换错误:", e);
        }
        return baseMapper.getSysLogPage(page, ew);
    }

    /**
     * 查询所有日志信息
     *
     * @param syslog
     *            参数列表
     * @return
     */
    @Override
    public List<SysLog> getSysLogList(SysLog syslog) {
        QueryWrapper<SysLog> ew = new QueryWrapper<SysLog>();
        ew.eq(StrUtil.isNotBlank(syslog.getType()), "type", syslog.getType())
            .like(StrUtil.isNotBlank(syslog.getTitle()), "title", syslog.getTitle())
            .likeRight(StrUtil.isNotBlank(syslog.getCreateBy()), "create_by", syslog.getCreateBy()).eq("del_flag", "0");
        try {
            if (StrUtil.isNotBlank(syslog.getBeginTime())) {
                ew.ge("create_time", DateUtil.parseDate(syslog.getBeginTime()));
            }
            if (StrUtil.isNotBlank(syslog.getEndTime())) {
                Date date = DateUtil.parseDate(syslog.getEndTime());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);
                ew.lt("create_time", calendar.getTime());
            }
        } catch (Exception e) {
            log.error("时间转换错误:", e);
        }
        return baseMapper.getSysLogList(ew);
    }
}
