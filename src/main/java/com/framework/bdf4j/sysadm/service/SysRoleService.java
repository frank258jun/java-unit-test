package com.framework.bdf4j.sysadm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.framework.bdf4j.sysadm.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 通过用户ID，查询角色信息
     *
     * @param userId
     * @return
     */
    List<SysRole> findRolesByUserId(Integer userId);

    /**
     * 通过角色ID，删除角色
     *
     * @param id
     * @return
     */
    Boolean removeRoleById(Integer id);

    /**
     * 简单输入参加相加，用于验证参数化测试
     *
     */
    int add(int input1, int input2);
}
