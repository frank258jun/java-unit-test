package com.framework.bdf4j.sysadm.controller;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.framework.bdf4j.annotations.SysLog;
import com.framework.bdf4j.comm.Constants;
import com.framework.bdf4j.comm.persist.PageDomain;
import com.framework.bdf4j.comm.util.R;
import com.framework.bdf4j.comm.util.StringUtils;
import com.framework.bdf4j.sysadm.entity.SysRole;
import com.framework.bdf4j.sysadm.service.SysRoleService;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/role")
public class SysRoleController {
    private final SysRoleService sysRoleService;
    
    /**
     * 添加角色
     *
     * @param sysRole
     *            角色信息
     * @return success、false
     */
    @SysLog("添加角色")
    @PostMapping
    @ResponseBody
    public R save(@RequestBody SysRole sysRole) {
        // 角色编码判重
        List<SysRole> rolelist = sysRoleService.list(Wrappers.<SysRole>query().lambda()
            .eq(SysRole::getRoleCode, sysRole.getRoleCode()).eq(SysRole::getDelFlag, Constants.STATUS_NORMAL));

        if (CollectionUtils.isNotEmpty(rolelist)) {
            return R.failed("角色编码不能重复");
        }
        return R.ok(sysRoleService.save(sysRole));
    }

    /**
     * 修改角色
     *
     * @param sysRole
     *            角色信息
     * @return success/false
     */
    @SysLog("修改角色")
    @PutMapping
    @ResponseBody
    public R update(SysRole sysRole) {
        // 角色编码判重
        List<SysRole> rolelist =
            sysRoleService.list(Wrappers.<SysRole>query().lambda().eq(SysRole::getRoleCode, sysRole.getRoleCode())
                .eq(SysRole::getDelFlag, Constants.STATUS_NORMAL).ne(SysRole::getRoleId, sysRole.getRoleId()));

        if (CollectionUtils.isNotEmpty(rolelist)) {
            return R.failed("角色编码不能重复");
        }
        sysRole.setUpdateTime(new Date());
        return R.ok(sysRoleService.updateById(sysRole));
    }

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    @SysLog("删除角色")
    @DeleteMapping("/{id}")
    @ResponseBody
    public R removeById(@PathVariable Integer id) {
        return R.ok(sysRoleService.removeRoleById(id));
    }

    /**
     * 根据ID查询角色信息
     *
     * @return 角色对象
     */
    @GetMapping("/{id}")
    @ResponseBody
    public R selectById(@PathVariable Integer id) {
        SysRole role = sysRoleService.getById(id);
        return R.ok(role);
    }

}
