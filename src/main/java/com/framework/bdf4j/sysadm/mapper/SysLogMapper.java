package com.framework.bdf4j.sysadm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.framework.bdf4j.comm.persist.ScaBaseMapper;
import com.framework.bdf4j.sysadm.entity.SysLog;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 *
 */
@Mapper
public interface SysLogMapper extends ScaBaseMapper<SysLog> {
    
    /**
     * 分页查询日志信息
     *
     * @param page
     *            分页对象
     * @param syslog
     *            参数列表
     * @return
     */
    IPage getSysLogPage(Page page, @Param("ew") QueryWrapper<SysLog> wrapper);

    /**
     * 查询所有日志信息
     *
     * @param syslog
     *            参数列表
     * @return
     */
    List<SysLog> getSysLogList(@Param("ew") QueryWrapper<SysLog> wrapper);
}
