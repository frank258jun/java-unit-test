package com.framework.bdf4j.sysadm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.framework.bdf4j.comm.persist.ScaBaseMapper;
import com.framework.bdf4j.sysadm.entity.SysRole;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 */
@Mapper
public interface SysRoleMapper extends ScaBaseMapper<SysRole> {
	/**
	 * 通过用户ID，查询角色信息
	 *
	 * @param userId
	 * @return
	 */
	List<SysRole> listRolesByUserId(Integer userId);
}
