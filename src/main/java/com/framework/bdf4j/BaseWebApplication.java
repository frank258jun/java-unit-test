package com.framework.bdf4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <b>文件名： </b>BaseWebApplication.java<br/>
 * <b>类描述： </b>管理平台启动类<br/>
 *
 */
@EnableTransactionManagement
@EnableCaching
@EnableAsync
@SpringBootApplication
public class BaseWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseWebApplication.class, args);
	}
}
