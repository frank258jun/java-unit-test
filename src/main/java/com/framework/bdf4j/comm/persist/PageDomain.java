package com.framework.bdf4j.comm.persist;

import lombok.Data;

/**
 * 分页数据
 * 
 */
@Data
public class PageDomain {
    /** 排序列 */
    private String sortName;
    /** 排序的方向 "desc" 或者 "asc". */
    private String sortOrder;
}
