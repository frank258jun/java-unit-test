package com.framework.bdf4j.comm.persist;

import java.time.LocalDateTime;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.framework.bdf4j.comm.web.WebUtil;

/**
 * <b>类描述： </b>自定义元对象字段填充控制器，实现公共字段自动写入<br/>
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        setFieldValByName("createTime",LocalDateTime.now(), metaObject);
        setFieldValByName("creator",WebUtil.getHttpCurrentUserId(), metaObject);
        setFieldValByName("createBy",WebUtil.getHttpCurrentUserId(), metaObject);
        setFieldValByName("updateBy",WebUtil.getHttpCurrentUserId(), metaObject);
        setFieldValByName("updateTime",LocalDateTime.now(), metaObject);
        setFieldValByName("version",0, metaObject);
    }

    /* 
     * 更新元对象字段填充（用于更新时对公共字段的填充）
     * @see com.baomidou.mybatisplus.mapper.MetaObjectHandler#updateFill(org.apache.ibatis.reflection.MetaObject)
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName("updateTime",LocalDateTime.now(), metaObject);
        setFieldValByName("updateBy",WebUtil.getHttpCurrentUserId(), metaObject);
        setFieldValByName("modifier",WebUtil.getHttpCurrentUserId(), metaObject);
    }
}
