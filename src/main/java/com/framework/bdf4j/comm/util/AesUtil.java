package com.framework.bdf4j.comm.util;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;

/**
 * AES算法
 * @version 1.0
 * @describle  目前只写了解密
 */
public class AesUtil {

    // AES 秘钥 应与前端定义一致
    private final static String key = "2020070221160001";

    // AES 偏移值 应与前端定义一致
    private final static String iv = "2020070221160001";

    private static AES aes = new AES(Mode.CTS, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());

    /**
     * AES 解密算法
     * @param preStr 待解密的字符串
     * @return
     */
    public static String decryptStr(String preStr) {

        //由于AES不满16位可能会填充空格，因此做了trim操作
        return aes.decryptStr(preStr).trim();
    }
}
