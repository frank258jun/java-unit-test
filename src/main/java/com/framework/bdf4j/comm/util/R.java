package com.framework.bdf4j.comm.util;

import java.io.Serializable;

import com.framework.bdf4j.comm.Constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 响应信息主体
 *
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int code;

	@Getter
	@Setter
	private String msg;


	@Getter
	@Setter
	private T data;

	public static <T> R<T> ok() {
		return restResult(null, Constants.SUCCESS, null);
	}

	public static <T> R<T> ok(T data) {
		return restResult(data, Constants.SUCCESS, null);
	}

	public static <T> R<T> ok(T data, String msg) {
		return restResult(data, Constants.SUCCESS, msg);
	}

	public static <T> R<T> failed() {
		return restResult(null, Constants.FAIL, null);
	}

	public static <T> R<T> failed(String msg) {
		return restResult(null, Constants.FAIL, msg);
	}

	public static <T> R<T> failed(T data) {
		return restResult(data, Constants.FAIL, null);
	}

	public static <T> R<T> failed(T data, String msg) {
		return restResult(data, Constants.FAIL, msg);
	}

	private static <T> R<T> restResult(T data, int code, String msg) {
		R<T> apiResult = new R<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(msg);
		return apiResult;
	}
}
