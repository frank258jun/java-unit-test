package com.framework.bdf4j.comm.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * <b>文件名： </b>DateConverter.java<br/>
 * <b>类描述： </b>SpringMvc的日期转换器<br/>
 */
public class DateConverter implements Converter<String, Date> {

	@Override    
	public Date convert(String source) {    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");    
	    dateFormat.setLenient(false); 
	    try {    
	        return dateFormat.parse(source);    
	    } catch (Exception e) {    
	        e.printStackTrace();    
	    }           
	    return null;    
	}
}
