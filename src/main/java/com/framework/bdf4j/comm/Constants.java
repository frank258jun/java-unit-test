package com.framework.bdf4j.comm;

/**
 * <b>文件名： </b>Constants.java<br/>
 * <b>类描述： </b>常量表<br/>
 */
public interface Constants {

	/** 项目默认编码格式 */
    public static final String DEFAULT_CHARSET = "UTF-8";
	/** 异常信息统一头信息 */
    public static final String EXCEPTION_HEAD = "非常遗憾的通知您,程序发生了异常 :";
    /** 当前用户ID */
    public static final String CURRENT_USERID = "CURRENT_USERID";
    /** 当前用户所属部门ID */
    public static final String CURRENT_DEPTID = "CURRENT_DEPTID";
    /** 当前用户所属部门父级ID */
    public static final String CURRENT_PARENTDEPTID = "CURRENT_PARENTDEPTID";
    /** 当前用户所属角色 */
    public static final String CURRENT_ROLELIST = "CURRENT_ROLELIST";
    /** 页面错误信息 */
    public static final String ERROR_MSG = "_error_msg";
    /** 当前登录用户名 */
    public static final String CURRENT_USERNAME = "CURRENT_USERNAME";
    /** 验证码KEY */
    public final static String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";
    /** 用户动态验证码 */
    public static final String USER_CHECKCODE = "USER_CHECKCODE";
    /**
     * 成功标记
     */
    Integer SUCCESS = 0;
    /**
     * 失败标记
     */
    Integer FAIL = 1;
    /**
     * 删除
     */
    String STATUS_DEL = "1";
    /**
     * 正常
     */
    String STATUS_NORMAL = "0";
    /**
     * 菜单
     */
    String MENU = "0";

    /**
     * 菜单树根节点
     */
    int MENU_TREE_ROOT_ID = -1;
    /**
     * 树根节点
     */
    int TREE_ROOT_ID = 0;
    /**
     * 菜单信息缓存
     */
    String MENU_DETAILS = "menu_details";
    /**
     * 字典信息缓存
     */
    String DICT_DETAILS = "dict_details";
    /**
     * 参数缓存
     */
    String PARAMS_DETAILS = "params_details";
    /**
     * 用户信息缓存
     */
    String USER_DETAILS = "user_details";
    /**
     * 默认租户ID
     */
    Integer TENANT_ID = 1;
}
