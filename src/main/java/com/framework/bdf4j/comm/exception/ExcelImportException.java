package com.framework.bdf4j.comm.exception;

public class ExcelImportException extends BaseException{

    public ExcelImportException(String msg) {
        super(msg);
    }

}
