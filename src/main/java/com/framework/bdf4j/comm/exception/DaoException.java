package com.framework.bdf4j.comm.exception;

/**
 * <b>文件名： </b>DaoException.java<br/>
 * <b>类描述： </b>DAO层抛出异常类<br/>
 */
public class DaoException extends BaseException {

	private static final long serialVersionUID = -3711290613973933714L;

	public DaoException(String msg) {
		super(msg);
	}

	public DaoException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}
}
