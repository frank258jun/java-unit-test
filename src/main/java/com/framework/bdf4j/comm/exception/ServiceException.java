package com.framework.bdf4j.comm.exception;

/**
 * <b>文件名： </b>ServiceException.java<br/>
 * <b>类描述： </b>Service层抛出异常类<br/>
 */
public class ServiceException extends BaseException {

	private static final long serialVersionUID = -3711290613973933714L;

	public ServiceException(String msg) {
		super(msg);
	}

	public ServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}
}
