package com.framework.bdf4j.comm.exception;

/**
 * <b>文件名： </b>BaseException.java<br/>
 * <b>类描述： </b>异常基类，各个模块的运行期异常均继承与该类<br/>
 */
public class BaseException extends RuntimeException {

	/**
	 * serialVersionUID : the serialVersionUID
	 */
	private static final long serialVersionUID = 1381325479896057076L;

	public BaseException(String msg) {
		super(msg);
	}

	public BaseException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public BaseException(Throwable cause) {
		super(cause);
	}
}
