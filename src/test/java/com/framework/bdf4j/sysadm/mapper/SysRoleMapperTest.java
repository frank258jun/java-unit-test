 package com.framework.bdf4j.sysadm.mapper;

 import  static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.framework.bdf4j.sysadm.entity.SysRole;

@RunWith(SpringRunner.class)
@SpringBootTest
class SysRoleMapperTest {

    @Autowired
    private SysRoleMapper sysRoleMapper;
    
    @Test
    void testListRolesByUserId() {
        List<SysRole> rolelist = sysRoleMapper.listRolesByUserId(1);
        System.out.println("打印测试结果1：" + rolelist);
        assertEquals(2, rolelist.size());
    }

    @Test
    void testSelectById() {
        SysRole role = sysRoleMapper.selectById(1);
        System.out.println("打印测试结果2：" + role);
        assertNotNull(role);
    }

}
