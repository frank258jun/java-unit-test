package com.framework.bdf4j.sysadm.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.alibaba.fastjson.JSONObject;
import com.framework.bdf4j.sysadm.entity.SysRole;

// MockMvc测试
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class SysRoleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Disabled
    @Test
    void testSave() throws Exception {
        SysRole sysRole = new SysRole();
        sysRole.setRoleName("test");
        sysRole.setRoleCode("role_test");
        String requestJson = JSONObject.toJSONString(sysRole);
        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.post("/admin/role").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
            .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
        result.getResponse().setCharacterEncoding("UTF-8");
        String resp = result.getResponse().getContentAsString();
        assertNotNull(resp);
    }

    @Disabled
    @Test
    void testRemoveById() throws Exception {
        MvcResult result =
            mockMvc.perform(MockMvcRequestBuilders.delete("/admin/role/3").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
        result.getResponse().setCharacterEncoding("UTF-8");
        String resp = result.getResponse().getContentAsString();
        assertNotNull(resp);
    }

    // @Disabled
    @Test
    void testSelectById() throws Exception {
        MvcResult result =
            mockMvc.perform(MockMvcRequestBuilders.get("/admin/role/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
        result.getResponse().setCharacterEncoding("UTF-8");
        String resp = result.getResponse().getContentAsString();
        assertNotNull(resp);
    }

}
