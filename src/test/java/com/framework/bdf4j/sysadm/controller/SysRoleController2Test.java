package com.framework.bdf4j.sysadm.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.framework.bdf4j.sysadm.entity.SysRole;
import com.framework.bdf4j.sysadm.service.SysRoleService;

// @WebMvcTest测试
@RunWith(SpringRunner.class)
@WebMvcTest(SysRoleController.class)
class SysRoleController2Test {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SysRoleService sysRoleService;

    //@Disabled
    @Test
    void testSelectById() throws Exception {
        SysRole role = new SysRole();
        role.setRoleId(5);
        role.setRoleCode("iamtest");
        // 模拟sysRoleService.getById(1)的行为
        when(sysRoleService.getById(1)).thenReturn(role);

        MvcResult result =
            mockMvc.perform(MockMvcRequestBuilders.get("/admin/role/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
        result.getResponse().setCharacterEncoding("UTF-8");
        String resp = result.getResponse().getContentAsString();
        assertNotNull(resp);
    }

}
