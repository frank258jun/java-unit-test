package com.framework.bdf4j.sysadm.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.framework.bdf4j.sysadm.entity.SysRole;
import com.framework.bdf4j.sysadm.service.SysRoleService;

// Mockito测试
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class SysRoleServiceImpl2Test {

    @Mock
    private SysRoleService sysRoleService;

    @Test
    void testGetById() {
        SysRole role = new SysRole();
        role.setRoleId(5);
        role.setRoleCode("iamtest");
        // 模拟sysRoleService.getById(1)的行为
        when(sysRoleService.getById(1)).thenReturn(role);
        assertEquals("iamtest", sysRoleService.getById(1).getRoleCode());
    }

}
