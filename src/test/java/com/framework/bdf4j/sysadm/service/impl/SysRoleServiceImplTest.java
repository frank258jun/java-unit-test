package com.framework.bdf4j.sysadm.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.framework.bdf4j.sysadm.entity.SysRole;
import com.framework.bdf4j.sysadm.service.SysRoleService;

//Spring Boot测试
@RunWith(SpringRunner.class)
@SpringBootTest
class SysRoleServiceImplTest {

    @Autowired
    private SysRoleService sysRoleService;

    @Test
    void testFindRolesByUserId() {
        List<SysRole> rolelist = sysRoleService.findRolesByUserId(1);
        System.out.println("打印测试结果：" + rolelist);
        assertEquals(2, rolelist.size());
    }

}
