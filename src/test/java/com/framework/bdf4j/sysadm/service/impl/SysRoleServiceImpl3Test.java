package com.framework.bdf4j.sysadm.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.framework.bdf4j.sysadm.service.SysRoleService;

// 参数化测试
@SpringBootTest
class SysRoleServiceImpl3Test {

    @Autowired
    private SysRoleService sysRoleService;

    @Disabled
    @ParameterizedTest
    @ValueSource(ints = {-1, -2, -3})
    public void testAdd(int num) {
        int result = sysRoleService.add(2, num);
        assertEquals(0, result);
    }

}
